﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RecoverClients;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Invoices.Services;
using Invoices.ValueObject;
using System.Configuration;
using Invoices.Entities;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://demo.mews.li/api/connector/v1/");
            Authentication authentication = new Authentication("E0D439EE522F44368DC78E1BFB03710C-D24FB11DBE31D4621C4817E028D9E1D", "C66EF7B239D24632943D115EDE9CB810-EA00F8FD8294692C940F6B5A8F9453D");
            CountryService Countries = new CountryService(client, authentication, "countries/getAll");
            List<Countries> countries = Countries.GetAllCountries().Result;
            foreach(Countries countryName in countries)
            {
                Console.WriteLine(countryName.EnglishName);
            }
            CompanyServices Companies = new CompanyServices(client, authentication, "companies/getAll");
            IEnumerable<Companies> companies = Companies.GetAllCompanies().Result;
            foreach(Companies company in companies)
            {
                Console.WriteLine(company.Name);
            }
            ServicesService ServicesService = new ServicesService(client, authentication, "services/getAll");
            IEnumerable<Service> Service = ServicesService.GetAllServices().Result;
            foreach(Service serv in Service)
            {
                Console.WriteLine(serv.Name);
            }
            BodyBills Body = new BodyBills("E0D439EE522F44368DC78E1BFB03710C-D24FB11DBE31D4621C4817E028D9E1D", "C66EF7B239D24632943D115EDE9CB810-EA00F8FD8294692C940F6B5A8F9453D", DateTime.UtcNow.AddYears(-1), DateTime.UtcNow);
            BillsService service = new BillsService(client, Body, "bills/getAllClosed");
            IEnumerable<Bills> Bill = service.GetAllBills().Result;
            List<string> Id = new List<string>();
            foreach (Bills bills in Bill)
            {
                Console.WriteLine($"id: {bills.Id}, State: {bills.State}, Date: {bills.IssuedUtc}");
                Id.Add(bills.CustomerId);
            }
            BodyCustomers BodyCustomers = new BodyCustomers("E0D439EE522F44368DC78E1BFB03710C-D24FB11DBE31D4621C4817E028D9E1D", "C66EF7B239D24632943D115EDE9CB810-EA00F8FD8294692C940F6B5A8F9453D", Id);
            CustomerService serviceCustomers = new CustomerService(client, BodyCustomers, "customers/getAllByIds");
            IEnumerable<Customers> Customers = serviceCustomers.GetCustomerById().Result;
            foreach (Customers customer in Customers)
            {
                Console.WriteLine(customer);
            }
            List<Invoice> invoices = new List<Invoice>();
            InvoiceFactory createInvoice = new InvoiceFactory();
            foreach (Bills bills in Bill)
            {
                createInvoice.bills = bills;
                createInvoice.customers = Customers.Where(x => x.Id == bills.CustomerId).First();
                invoices.Add(createInvoice.Build());
            }
            foreach (Invoice invoice in invoices)
            {
                Console.WriteLine($"CustomerId: {invoice.CustomerId}, CustomerName: {invoice.CustomerFirtsName}, CustomerLastName: {invoice.CustomerLastName}, Nationality: {countries.Where(x=>x.Code==invoice.CustomerCountry).FirstOrDefault()?.EnglishName}");
            }
            Excel.Application newExel = new Excel.Application();
            if (newExel == null)
            {
                return;
            }
            Excel.Workbook newExcelWorkBook;
            Excel.Worksheet newExcelWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            newExcelWorkBook = newExel.Workbooks.Add(misValue);
            newExcelWorkSheet = (Excel.Worksheet)newExcelWorkBook.Worksheets[1];
            newExcelWorkSheet.Cells[1, 1] = nameof(Invoice.CustomerId);
            newExcelWorkSheet.Cells[1, 2] = nameof(Invoice.CustomerId);
            newExcelWorkSheet.Cells[1, 3] = nameof(Invoice.CustomerLastName);
            newExcelWorkSheet.Cells[1, 4] = nameof(Invoice.CustomerCountry);
            int cell = 2;
            foreach (Invoice invoice in invoices)
            {
                newExcelWorkSheet.Cells[cell, 1] = invoice.CustomerId;
                newExcelWorkSheet.Cells[cell, 2] = invoice.CustomerFirtsName;
                newExcelWorkSheet.Cells[cell, 3] = invoice.CustomerLastName;
                newExcelWorkSheet.Cells[cell, 4] = countries.Where(x => x.Code == invoice.CustomerCountry).FirstOrDefault()?.EnglishName;
                cell++;
            }
            newExcelWorkBook.SaveAs("C:\\Users\\user\\Downloads\\Facturas1.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            newExcelWorkBook.Close(true, misValue, misValue);
            newExel.Quit();
            Marshal.ReleaseComObject(newExcelWorkSheet);
            Marshal.ReleaseComObject(newExcelWorkBook);
            Marshal.ReleaseComObject(newExel);
            Console.WriteLine("Se ha creado correctamente.");
            Console.ReadLine();
        }
    }
}
