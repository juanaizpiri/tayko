﻿namespace InvoiceToExcel
{
    partial class FacturasEnExcel
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.DTPDesde = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DTPHasta = new System.Windows.Forms.DateTimePicker();
            this.btGeneralExcel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DTPDesde
            // 
            this.DTPDesde.Location = new System.Drawing.Point(13, 78);
            this.DTPDesde.Name = "DTPDesde";
            this.DTPDesde.Size = new System.Drawing.Size(277, 22);
            this.DTPDesde.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Desde:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Hasta:";
            // 
            // DTPHasta
            // 
            this.DTPHasta.Location = new System.Drawing.Point(13, 196);
            this.DTPHasta.Name = "DTPHasta";
            this.DTPHasta.Size = new System.Drawing.Size(277, 22);
            this.DTPHasta.TabIndex = 2;
            // 
            // btGeneralExcel
            // 
            this.btGeneralExcel.Location = new System.Drawing.Point(116, 278);
            this.btGeneralExcel.Name = "btGeneralExcel";
            this.btGeneralExcel.Size = new System.Drawing.Size(127, 23);
            this.btGeneralExcel.TabIndex = 4;
            this.btGeneralExcel.Text = "Generar Excel";
            this.btGeneralExcel.UseVisualStyleBackColor = true;
            this.btGeneralExcel.Click += new System.EventHandler(this.btGeneralExcel_Click);
            // 
            // FacturasEnExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 348);
            this.Controls.Add(this.btGeneralExcel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DTPHasta);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DTPDesde);
            this.Name = "FacturasEnExcel";
            this.Text = "Facturas En Excel";
            this.Load += new System.EventHandler(this.FacturasEnExcel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker DTPDesde;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker DTPHasta;
        private System.Windows.Forms.Button btGeneralExcel;
    }
}

