﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net.Http;
using Invoices.Entities;
using Invoices.Services;
using Invoices.ValueObject;

namespace InvoiceToExcel
{
    public partial class FacturasEnExcel : Form
    { 
        public FacturasEnExcel()
        {
            InitializeComponent();
        }

        private async void btGeneralExcel_Click(object sender, EventArgs e)
        {
            DateTime Desde = DTPDesde.Value;
            DateTime Hasta = DTPHasta.Value;
            if (DateTime.Compare(Desde.Date, Hasta.Date) != 0&& DateTime.Compare(Desde.Date, Hasta.Date)<0)
            {
                HttpClient Client = new HttpClient();
                Client.BaseAddress = new Uri("https://www.mews.li//api/connector/v1/");
                Authentication Authentication = new Authentication(ConfigurationManager.AppSettings["ClientToken"], ConfigurationManager.AppSettings["AccessToken"]);
                CompanyServices GetCompanies = new CompanyServices(Client, Authentication, "companies/getAll");
                IEnumerable<Companies> AllCompanies = await GetCompanies.GetAllCompanies();
                CountryService GetCountries = new CountryService(Client, Authentication, "countries/getAll");
                IEnumerable<Countries> AllCountries = await GetCountries.GetAllCountries();
                ServicesService GetServices = new ServicesService(Client, Authentication, "services/getAll");
                IEnumerable<Service> AllServices = await GetServices.GetAllServices();
                BodyBills bodyBills = new BodyBills(ConfigurationManager.AppSettings["ClientToken"], ConfigurationManager.AppSettings["AccessToken"], Desde, Hasta);
                BillsService GetClosedBills = new BillsService(Client, bodyBills, "bills/getAllClosed");
                IEnumerable<Bills> AllBills = await GetClosedBills.GetAllBills();
                List<string> IdCustomers = new List<string>();
                foreach (Bills Bill in AllBills)
                {
                    IdCustomers.Add(Bill.CustomerId);
                }
                BodyCustomers bodyCustomers = new BodyCustomers(ConfigurationManager.AppSettings["ClientToken"], ConfigurationManager.AppSettings["AccessToken"], IdCustomers);
                CustomerService GetCustomers = new CustomerService(Client, bodyCustomers, "customers/getAllByIds");
                IEnumerable<Customers> AllCustomers = await GetCustomers.GetCustomerById();
                List<Invoice> AllInvoices = new List<Invoice>();
                InvoiceFactory createInvoice = new InvoiceFactory();
                foreach (Bills bills in AllBills)
                {
                    createInvoice.bills = bills;
                    createInvoice.customers = AllCustomers.Where(x => x.Id == bills.CustomerId).First();
                    AllInvoices.Add(createInvoice.Build());
                }
                GenerateExcelService GenerateExel = new GenerateExcelService(AllInvoices, AllCountries, AllCompanies, AllServices, AllBills, AllCustomers);
                if (GenerateExel.Generar() == 1)
                {
                    MessageBox.Show("Se creo el excel correctamente.");
                }
                else
                {
                    MessageBox.Show("Fallo!");
                }
            }
            else
            {
                MessageBox.Show("Tienes que introducir dos fechas distintas y la fecha desde tiene que ser mas pequeña.");
            }
        }

        private void FacturasEnExcel_Load(object sender, EventArgs e)
        {

        }
    }
}
