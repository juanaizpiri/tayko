﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoices.Entities
{
    public class Bills
    {
        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string CompanyId { get; set; }
        public string State { get; set; }
        public string Type { get; set; }
        public string Number { get; set; }
        public string IssuedUtc { get; set; }
        public string DueUtc { get; set; }
        public List<Revenue> Revenue { get; set; }
        public List<Payment> Payments { get; set; }
    }
}
