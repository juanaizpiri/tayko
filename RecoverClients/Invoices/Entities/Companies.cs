﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invoices.ValueObject;

namespace Invoices.Entities
{
    public class Companies
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Number { get; set; }
        public string Indentifier { get; set; }
        public string TaxIdentificationNumber { get; set; }
        public Address Address { get; set; }
    }
}
