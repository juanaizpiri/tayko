﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoices.Entities
{
    public class CountryAlliances
    {
        public string Code { get; set; }
        public string EnglishName { get; set; }
        public string[] CountryCodes { get; set; }
    }
}
