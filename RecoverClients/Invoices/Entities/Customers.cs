﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invoices.ValueObject;

namespace Invoices.Entities
{
    public class Customers
    {
        public string Id { get; set; }
        public string Number { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SecondLastName { get; set; }
        public string Tittle { get; set; }
        public string Gender { get; set; }
        public string NationalityCode { get; set; }
        public string LanguageCode { get; set;}
        public string BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string TaxIdentificationNumber { get; set; }
        public string LoyaltyCode { get; set; }
        public string Notes { get; set; }
        public string[] Classifications { get; set; }
        public string[] Options { get; set; }
        public Document Passport { get; set; }
        public Document IdentityCard { get; set; }
        public Document Visa { get; set; }
        public Document DriversLicense { get; set; }
        public Address Address { get; set; }
        public string CreateUtc { get; set; }
        public string UpdateUtc { get; set; }
    }
}
