﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoices.Entities
{
    public class Invoice
    {
        public Invoice(Bills bills, Customers customers)
        {
            CustomerId = customers.Id;
            CustomerNumber = customers.Number;
            CustomerFirtsName = customers.FirstName;
            CustomerLastName = customers.LastName;
            CustomerSecondLastName = customers.SecondLastName;
            CustomerCountry = customers.NationalityCode;
            CompanyId = bills.CompanyId;
            InvoiceNumber = bills.Number;
            InvoiceDate = bills.IssuedUtc;
            if(customers.IdentityCard!=null)
            {
                DocumentType = "DNI";
                DocumentNumber = customers.IdentityCard.Number;
            }
            else if(customers.Passport!=null)
            {
                DocumentType = "Pasaporte";
                DocumentNumber = customers.Passport.Number;
            }
            else if(customers.Visa!=null)
            {
                DocumentType = "Visa";
                DocumentNumber = customers.Visa.Number;
            }
            else if(customers.DriversLicense!=null)
            {
                DocumentType = "Carnet de conducir";
                DocumentNumber = customers.DriversLicense.Number;
            }
            foreach(Revenue revenue in bills.Revenue)
            {
                RevenueInvoices revenueInvoices = new RevenueInvoices(revenue);
                Revenues.Add(revenueInvoices);
            }
            foreach(Payment payments in bills.Payments)
            {
                InvoicePayment invoicePayment = new InvoicePayment(payments);
                Payment.Add(invoicePayment);
            }
        }
        public string InvoiceNumber { get; set; }
        public string CustomerId { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerFirtsName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerSecondLastName { get; set; }
        public string CustomerCountry { get; set; }
        public string DocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public string CompanyId { get; set; }
        public string InvoiceDate { get; set; }
        public List<RevenueInvoices> Revenues { get; set; } = new List<RevenueInvoices>();
        public List<InvoicePayment> Payment { get; set; } = new List<InvoicePayment>();
    }
}
