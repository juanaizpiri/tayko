﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invoices.ValueObject;

namespace Invoices.Entities
{
    public class InvoicePayment
    {
        public InvoicePayment(Payment payment)
        {
            Id = payment.Id;
            BillId = payment.BillId;
            Name = payment.Name;
            Amounts = payment.Amount;
            Date = payment.ConsumptionUtc;
        }
        public string Id { get; set; }
        public string BillId { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public Amount Amounts { get; set; }
    }
}
