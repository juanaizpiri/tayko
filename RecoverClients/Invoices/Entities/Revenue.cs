﻿using Invoices.ValueObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoices.Entities
{
    public class Revenue
    {
        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string ServiceId { get; set; }
        public string BillId { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string ConsumptionUtc { get; set; }
        public string ClosedUtc { get; set; }
        public string State { get; set; }
        public Amount Amount { get; set; }
    }
}
