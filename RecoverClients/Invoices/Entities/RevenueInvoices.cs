﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invoices.ValueObject;

namespace Invoices.Entities
{
    public class RevenueInvoices
    {
        public RevenueInvoices(Revenue revenue)
        {
            Id = revenue.Id;
            BillId = revenue.BillId;
            Name = revenue.Name;
            Amounts = revenue.Amount;
            ServiceId = revenue.ServiceId;
        }
        public string Id { get; set; }
        public string BillId { get; set; }
        public string Name { get; set; }
        public string ServiceId { get; set; }
        public Amount Amounts { get; set; }
    }
}
