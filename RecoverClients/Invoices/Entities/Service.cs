﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invoices.ValueObject;

namespace Invoices.Entities
{
    public class Service
    {
        public string Id { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public Promotions Promotions { get; set; }
        public string Type { get; set; }
        public bool IsSet { get; set; }
    }
}
