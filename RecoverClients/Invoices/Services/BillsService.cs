﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Invoices.ValueObject;
using Invoices.Entities;

namespace Invoices.Services
{
    public class BillsService
    {
        
        readonly HttpClient Client;
        readonly BodyBills body;
        readonly string Url;

        public BillsService(HttpClient client, BodyBills body, string url)
        {
            Client = client ?? throw new ArgumentNullException(nameof(client));
            this.body = body ?? throw new ArgumentNullException(nameof(body));
            Url = url ?? throw new ArgumentNullException(nameof(url));
        }

        private class ResultBills
        {
            public Bills[] Bills { get; set; }
        }
        public async Task<IEnumerable<Bills>> GetAllBills()
        {
            ResultBills bills = null;
            HttpResponseMessage response = await Client.PostAsJsonAsync(Url, body);
            response.EnsureSuccessStatusCode();
            bills = await response.Content.ReadAsAsync<ResultBills>();
            return bills.Bills;
        }
    }
}
