﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Invoices.ValueObject;
using Invoices.Entities;

namespace Invoices.Services
{
    public class CompanyServices
    {
        readonly HttpClient Client;
        readonly Authentication Authentication;
        readonly string Url;
        private IEnumerable<Companies> CompaniesCache = null;
        public CompanyServices(HttpClient client, Authentication authentication, string url)
        {
            Client = client ?? throw new ArgumentNullException(nameof(client));
            Authentication = authentication ?? throw new ArgumentNullException(nameof(authentication));
            Url = url ?? throw new ArgumentNullException(nameof(url));
        }
        public class resultCompanies
        {
            public Companies[] Companies { get; set; }
        }
        public async Task<IEnumerable<Companies>> GetAllCompanies()
        {
            if (CompaniesCache==null)
            {
                resultCompanies Companies = new resultCompanies();
                HttpResponseMessage response = await Client.PostAsJsonAsync(Url, Authentication);
                response.EnsureSuccessStatusCode();
                Companies = await response.Content.ReadAsAsync<resultCompanies>();
                CompaniesCache = Companies.Companies;
                return CompaniesCache;
            }
            else
            {
                return CompaniesCache;
            }
        }
    }
}
