﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Invoices.ValueObject;
using Invoices.Entities;

namespace Invoices.Services
{
    public class CountryService
    {
        readonly HttpClient Client;
        readonly Authentication Authentication;
        readonly string Url;
        private List<Countries> CountriesCache = null;

        public CountryService(HttpClient client, Authentication authentication, string url)
        {
            Client = client ?? throw new ArgumentNullException(nameof(client));
            Authentication = authentication ?? throw new ArgumentNullException(nameof(authentication));
            Url = url ?? throw new ArgumentNullException(nameof(url));
        }
        public class ResultCountries
        {
            public List<Countries> Countries { get; set; } = new List<Countries>();
            public List<CountrySubDivisions> CountrySubDivisions { get; set; } = new List<CountrySubDivisions>();
            public List<CountryAlliances> CountryAlliances { get; set; } = new List<CountryAlliances>();
        }
        public async Task<List<Countries>> GetAllCountries()
        {
            if (CountriesCache == null)
            {
                ResultCountries Countries = new ResultCountries();
                HttpResponseMessage response = await Client.PostAsJsonAsync(Url, Authentication);
                response.EnsureSuccessStatusCode();
                Countries = await response.Content.ReadAsAsync<ResultCountries>();
                CountriesCache= Countries.Countries;
                return CountriesCache;
            }
            else
            {
                return CountriesCache;
            }
        }
    }
}
