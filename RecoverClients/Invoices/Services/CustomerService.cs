﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Invoices.ValueObject;
using Invoices.Entities;

namespace Invoices.Services
{
    public class CustomerService
    {
        readonly HttpClient Client;
        readonly BodyCustomers Body;
        readonly string Url;

        public CustomerService(HttpClient client, BodyCustomers body, string url)
        {
            Client = client ?? throw new ArgumentNullException(nameof(client));
            Body = body ?? throw new ArgumentNullException(nameof(body));
            Url = url ?? throw new ArgumentNullException(nameof(url));
        }
        private class ResultCustomers
        {
            public Customers[] Customers { get; set; }
        }
        public async Task<IEnumerable<Customers>> GetCustomerById()
        {
            ResultCustomers Customer = null;
            HttpResponseMessage response = await Client.PostAsJsonAsync(Url, Body);
            response.EnsureSuccessStatusCode();
            Customer = await response.Content.ReadAsAsync<ResultCustomers>();
            return Customer.Customers;
        }

    }
}
