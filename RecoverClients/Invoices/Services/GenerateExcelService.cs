﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using Invoices.Entities;
using Invoices.Services;
using Invoices.ValueObject;
using System.Runtime.InteropServices;
using Invoices.ViewModels;
using Microsoft.Office.Interop.Excel;

namespace Invoices.Services
{
    public class GenerateExcelService
    {
        private IEnumerable<Invoice> AllInvoices { get; }
        private IEnumerable<Countries> AllCountries { get; }
        private IEnumerable<Companies> AllCompanies { get; }
        private IEnumerable<Service> AllServices { get; set; }
        private IEnumerable<Bills> AllBills { get; }
        private IEnumerable<Customers> AllCustomers { get; }
        public GenerateExcelService(IEnumerable<Invoice> allInvoices, IEnumerable<Countries> allCountries, IEnumerable<Companies> allCompanies, IEnumerable<Service> allServices, IEnumerable<Bills> allBills, IEnumerable<Customers> allCustomers)
        {
            AllInvoices = allInvoices;
            AllCountries = allCountries;
            AllCompanies = allCompanies;
            AllServices = allServices;
            AllBills = allBills;
            AllCustomers = allCustomers;
        }
        public int Generar()
        {
            Excel.Application newExel = new Excel.Application();
            if (newExel == null)
            {
                return 0;
            }
            UsedServices used = new UsedServices(AllServices, AllInvoices);
            MaxPaymets maxpayments = new MaxPaymets(AllInvoices);
            int max = maxpayments.Max();
            AllServices = used.Used();
            object[,] cargar = new object [AllInvoices.Count()+1,(14+(3*(AllServices.Where(x=>x.IsSet==true).Count()))+(max*3))+1];
            Excel.Workbook newExcelWorkBook;
            Excel.Worksheet newExcelWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            newExcelWorkBook = newExel.Workbooks.Add(misValue);
            newExcelWorkSheet = (Excel.Worksheet)newExcelWorkBook.Worksheets[1];
            cargar[0,0] = "Nº Factura"; 
            cargar[0,1] = "Nº Cliente";
            cargar[0,2] = "Tipo Documento";
            cargar[0,3] = "Nº Documento";
            cargar[0,4] = "Nombre Cliente";
            cargar[0,5] = "Apellidos Cliente";
            cargar[0,6] = "Pais Cliente";
            cargar[0,7] = "Nombre Empresa";
            cargar[0,8] = "CIF Empresa";
            cargar[0,9] = "Direccion Empresa";
            cargar[0,10] = "Fecha Factura";
            int posicion = 10;
            foreach(Service service in AllServices)
            {
                if (service.IsSet == true)
                {
                    cargar[0, posicion + 1] = $"Base Imp {service.Name}";
                    cargar[0, posicion + 2] = $"Iva {service.Name}";
                    cargar[0, posicion + 3] = $"Total {service.Name}";
                    posicion += 3;
                }
            }
            cargar[0,posicion + 1] = "Base Imp";
            cargar[0,posicion + 2] = "Iva";
            cargar[0, posicion + 3] = "Total";
            posicion = posicion + 3;
            for (int i = 0; i < max; i++)
            {
                cargar[0, posicion + 1] = "Metodo de pago";
                cargar[0, posicion + 2] = "Cantidad Pagada";
                cargar[0, posicion + 3] = "Fecha pago creado";
                posicion = posicion + 3;
            }
            cargar[0,posicion + 1] = "Balance";
            int cell = 1;
            foreach (Invoice invoice in AllInvoices)
            {
                cargar[cell, 0] = invoice.InvoiceNumber;
                cargar[cell,1] = invoice.CustomerNumber;
                cargar[cell,2] = invoice.DocumentType;
                cargar[cell,3] = invoice.DocumentNumber;
                cargar[cell,4] = invoice.CustomerFirtsName;
                cargar[cell,5] = invoice.CustomerLastName + " " + invoice.CustomerSecondLastName;
                cargar[cell,6]= AllCountries.Where(x => x.Code == invoice.CustomerCountry).FirstOrDefault()?.EnglishName;
                cargar[cell,7] = AllCompanies.Where(x => x.Id == invoice.CompanyId).FirstOrDefault()?.Name;
                cargar[cell, 8] = AllCompanies.Where(x => x.Id == invoice.CompanyId).FirstOrDefault()?.TaxIdentificationNumber;
                ViewModelAdress CompanyAdress = new ViewModelAdress(AllCompanies.Where(x => x.Id == invoice.CompanyId).FirstOrDefault()?.Address, AllCountries);
                cargar[cell,9] = CompanyAdress.ToString();
                cargar[cell,10] = invoice.InvoiceDate;
                int position = 10;
                decimal? ValueTotal = 0;
                decimal? NetTotal = 0;
                decimal? TaxTotal = 0;
                foreach(Service service in AllServices)
                {
                    if (service.IsSet == true)
                    {
                        decimal? Value = 0;
                        decimal? Net = 0;
                        decimal? Tax = 0;
                        foreach (RevenueInvoices revenue in invoice.Revenues)
                        {
                            if (service.Id == revenue.ServiceId)
                            {
                                Value = Value + revenue.Amounts.Value;
                                Net = Net + revenue.Amounts.Net;
                                Tax = Tax + revenue.Amounts.Tax;
                            }
                        }
                        if (Net != 0)
                        {
                            cargar[cell, position + 1] = Net;
                            cargar[cell, position + 2] = Tax;
                            cargar[cell, position + 3] = Value;
                        }
                        else
                        {
                            cargar[cell, position + 1] = "";
                            cargar[cell, position + 2] = "";
                            cargar[cell, position + 3] = "";
                        }
                        ValueTotal += Value;
                        NetTotal += Net;
                        TaxTotal += Tax;
                        position += 3;
                    }
                }
                cargar[cell,position + 1] = NetTotal;
                cargar[cell,position + 2] = TaxTotal;
                cargar[cell,position + 3] = ValueTotal;
                position = position + 3;
                decimal? totalPayd = 0;
                if (invoice.Payment.Count > 0)
                {
                    for (int i = 0; i < max; i++)
                    {
                        if (invoice.Payment.Count > i)
                        {
                            cargar[cell, position + 1] = invoice.Payment[i].Name;
                            cargar[cell, position + 2] = invoice.Payment[i].Amounts.Value;
                            cargar[cell, position + 3] = invoice.Payment[i].Date;
                            totalPayd = totalPayd + invoice.Payment[i].Amounts.Value;
                        }
                        else
                        {
                            cargar[cell, position + 1] = "";
                            cargar[cell, position + 2] = "";
                            cargar[cell, position + 3] = "";
                        }
                        position = position + 3;
                    }
                    cargar[cell, position + 1] = (ValueTotal + totalPayd);
                }
                else
                {
                    for (int i = 0; i < max; i++)
                    {
                            cargar[cell, position + 1] = "";
                            cargar[cell, position + 2] = "";
                            cargar[cell, position + 3] = "";
                        position = position + 3;
                    }
                    cargar[cell,position + 1] = (ValueTotal + 0);
                }
                cell++;
            }
            var startCell = (Range)newExcelWorkSheet.Cells[1, 1];
            var endCell = (Range)newExcelWorkSheet.Cells[AllInvoices.Count()+1, (14 + (3 * (AllServices.Where(x => x.IsSet == true).Count())) + (max * 3))+1];
            var endCellColor = (Range)newExcelWorkSheet.Cells[1, (14 + (3 * (AllServices.Where(x => x.IsSet == true).Count())) + (max * 3)) + 1];
            var writeRange = newExcelWorkSheet.Range[startCell, endCell];
            writeRange.Value2 = cargar;
            writeRange.Columns.AutoFit();
            writeRange.NumberFormat="@";
            newExcelWorkBook.SaveAs(Environment.CurrentDirectory+"FacturasTAYKO.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            newExcelWorkBook.Close(true, misValue, misValue);
            newExel.Quit();
            Marshal.ReleaseComObject(newExcelWorkSheet);
            Marshal.ReleaseComObject(newExcelWorkBook);
            Marshal.ReleaseComObject(newExel);
            return 1;
        }
    }
}
