﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invoices.Entities;

namespace Invoices.Services
{
    class MaxPaymets
    {
        private IEnumerable<Invoice> Allinvoices { get; }
        private int maxPayments;
        public MaxPaymets(IEnumerable<Invoice> allinvoices)
        {
            Allinvoices = allinvoices;
        }
        public int Max()
        {
            maxPayments = 0;
            foreach(Invoice invoice in Allinvoices)
            {
                if(maxPayments<invoice.Payment.Count())
                {
                    maxPayments = invoice.Payment.Count();
                }
            }
            return maxPayments;
        }
    }
}
