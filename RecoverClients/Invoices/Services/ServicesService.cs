﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Invoices.ValueObject;
using Invoices.Entities;

namespace Invoices.Services
{
    public class ServicesService
    {
        readonly HttpClient Client;
        readonly Authentication authentication;
        readonly string Url;
        private IEnumerable<Service> ServicesCache = null;
        public ServicesService(HttpClient client, Authentication authentication, string url)
        {
            Client = client ?? throw new ArgumentNullException(nameof(client));
            this.authentication = authentication ?? throw new ArgumentNullException(nameof(authentication));
            Url = url ?? throw new ArgumentNullException(nameof(url));
        }
        public class ResultServices
        {
            public Service[] Services { get; set; }
        }
        public async Task<IEnumerable<Service>> GetAllServices()
        {
            if(ServicesCache==null)
            {
                ResultServices resultServices = new ResultServices();
                HttpResponseMessage response = await Client.PostAsJsonAsync(Url, authentication);
                response.EnsureSuccessStatusCode();
                resultServices = response.Content.ReadAsAsync<ResultServices>().Result;
                ServicesCache = resultServices.Services;
                return ServicesCache;
            }
            else
            {
                return ServicesCache;
            }
        }
    }
}
