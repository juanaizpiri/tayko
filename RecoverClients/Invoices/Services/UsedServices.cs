﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invoices.Entities;

namespace Invoices.Services
{
    class UsedServices
    {
        private IEnumerable<Service> Allservices { get; }
        private IEnumerable<Invoice> Allinvoices { get; }
        public UsedServices(IEnumerable<Service> allservices, IEnumerable<Invoice> allinvoices)
        {
            Allservices = allservices;
            Allinvoices = allinvoices;
        }
        public IEnumerable<Service> Used()
        {
            foreach(Invoice invoice in Allinvoices)
            {
                foreach(RevenueInvoices revenue in invoice.Revenues)
                {
                    Allservices.Where(x => x.Id == revenue.ServiceId).FirstOrDefault().IsSet =true;
                }
            }
            return Allservices;
        }
    }
}
