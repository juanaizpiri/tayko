﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoices.ValueObject
{
    public class Amount
    {
        public string Currency { get; set; }
        public decimal? Value { get; set; }
        public decimal? Net { get; set; }
        public decimal? Tax { get; set; }
        public decimal? TaxRate { get; set; }
    }
}
