﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoices.ValueObject
{
    public class BodyBills : Authentication
    {
        public BodyBills(string clientToken, string accessToken, DateTime startUtc, DateTime endUtc) : base(clientToken, accessToken)
        {
            StartUtc = startUtc;
            EndUtc = endUtc;
        }
        public BodyBills(Authentication authentication, DateTime startUtc, DateTime endUtc):base(authentication)
        {
            StartUtc = startUtc;
            EndUtc = endUtc;
        }
        public DateTime StartUtc { get; }
        public DateTime EndUtc { get; }
    }
}
