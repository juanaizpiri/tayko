﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoices.ValueObject
{
    public class BodyCustomers : Authentication
    {
        public BodyCustomers(Authentication authentication, List<string> id) : base(authentication)
        {
            CustomerIds = id;
        }

        public BodyCustomers(string clientToken, string accessToken, List<string> id) : base(clientToken, accessToken)
        {
            CustomerIds = id;
        }

        public List<string> CustomerIds { get; }
    }
}
