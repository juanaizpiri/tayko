﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoices.ValueObject
{
    public class Document
    {
        public string Number { get; set; }
        public string Expiration { get; set; }
        public string Issuance { get; set; }
        public string IssuingCountryCode { get; set; }
    }
}
