﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invoices.Entities;

namespace Invoices.ValueObject
{
    public class InvoiceFactory
    {
        public Customers customers { get; set; }
        public Bills bills { get; set; }
        public Invoice Build()
        {
            return new Invoice(bills, customers);
        }
    }
}
