﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoices.ValueObject
{
    public class Promotions
    {
        public bool BeforeCheckIn { get; set; }
        public bool AfterCheckIn { get; set; }
        public bool DuringStay { get; set; }
        public bool BeforeCheckOut { get; set; }
        public bool AfterCehckOut { get; set; }
    }
}
