﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoices.ValueObject
{
    public class Authentication
    {
        public Authentication(string clientToken, string accessToken)
        {
            ClientToken = clientToken ?? throw new ArgumentNullException(nameof(clientToken));
            AccessToken = accessToken ?? throw new ArgumentNullException(nameof(accessToken));
        }
        public Authentication(Authentication authentication):this(authentication.ClientToken, authentication.AccessToken)
        {
            
        }
        public string ClientToken { get;}
        public string AccessToken { get; }
    }
}
