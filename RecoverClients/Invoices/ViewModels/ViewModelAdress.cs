﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invoices.ValueObject;
using Invoices.Entities;

namespace Invoices.ViewModels
{
    class ViewModelAdress
    {
        public  ViewModelAdress(Address companyAdress, IEnumerable<Countries> AllCountries)
        {
            Address = companyAdress?.Line1 + " " + companyAdress?.PostalCode + " " + companyAdress?.City + " " + AllCountries.Where(x => x.Code == companyAdress?.CountryCode).FirstOrDefault()?.EnglishName;
        }
        public string Address { get;}
        public override string ToString()
        {
            return Address.ToString();
        }
    }
}
