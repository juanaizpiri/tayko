﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RecoverClients
{
    public class Recover
    {
        static HttpClient cliente = new HttpClient();
        Tokens obj = new Tokens
        {
            ClientToken = "E0D439EE522F44368DC78E1BFB03710C-D24FB11DBE31D4621C4817E028D9E1D",
            AccessToken = "C66EF7B239D24632943D115EDE9CB810-EA00F8FD8294692C940F6B5A8F9453D",
        };
        public class Currencies
        {
            public string Code { get; set; }
            public decimal Precision { get; set; }
        }
        public class CurrenciesResult
        {
            public Currencies[] Currencies { get; set; }
        }
        public class Tokens
        {
            public string ClientToken { get; set; }
            public string AccessToken { get; set; }
        }
        public async Task<CurrenciesResult> APICurrencies()
        {
            CurrenciesResult currencies = null;
            HttpResponseMessage response = await cliente.PostAsJsonAsync(
                "https://demo.mews.li/api/connector/v1/currencies/getAll", obj);
            response.EnsureSuccessStatusCode();
            currencies=await response.Content.ReadAsAsync<CurrenciesResult>();
            return currencies;
        }
        public void resultado()
        {
            CurrenciesResult resultado = APICurrencies().Result;
            foreach(Currencies a in resultado.Currencies)
            {
                Console.WriteLine("Code: {0}", a.Code);
                Console.WriteLine("Precision: {0}", a.Precision);
            }
        }

    }
}
